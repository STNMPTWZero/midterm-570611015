//
//  TBtab2ViewController.swift
//  MidtermExam
//
//  Created by Thanisorn Jundee on 3/22/2560 BE.
//  Copyright © 2560 Thanisorn Jundee. All rights reserved.
//

import UIKit

class TBtab2ViewController: UIViewController {

    @IBOutlet weak var userid: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        userid.text = UserDefaults.standard.value(forKey: "user") as! String?
    }


}
