//
//  TBtab1ViewController.swift
//  MidtermExam
//
//  Created by Thanisorn Jundee on 3/22/2560 BE.
//  Copyright © 2560 Thanisorn Jundee. All rights reserved.
//

import UIKit

class TBtab1ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let musiclist = ["Hello",
                     "Come to me",
                     "Cold water","Where Them Girls At",
                     "Blow Your Mind (Mwah)",
                     "Love on Me",
                     "Thinking About You",
                     "Keep it Mello",
                     "One Night Only",
                     "The Heart Wants What It Wants"]
    override func viewDidLoad() {
        super.viewDidLoad()
         print(UserDefaults.standard.string(forKey: "user")!)
    }
    @IBAction func back(_ sender: Any) {
        UserDefaults.standard.setValue(nil, forKey: "user")
        dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musiclist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = "\(indexPath.item + 1)  \(musiclist[indexPath.item])"
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "detail") as! TBdetailViewController
        vc.number = indexPath.item
        self.present(vc, animated: true, completion: nil)
    }

}
